import React, { Fragment } from 'react'
import { View, Text, Image } from 'react-native'
import { styles } from '../../styles/components/TrendsList'

export const TrendsList = ({trendsList}) => {
    return (
        <Fragment>
            {
                trendsList.map((value, index) => {
                    return (
                        <View style={styles.transactionItem} key={index}>
                            <View style={styles.imageContainer}>
                                <Image
                                    source={value.icon}
                                    style={styles.icon}
                                />
                                <View style={styles.mainText}>
                                    <Text style={styles.textHead}>{value.headText}</Text>
                                    <Text style={styles.textFoot}>{value.footText}</Text>
                                </View>
                            </View>
                            <View>
                                <Text style={styles.amountText}>{value.amount}</Text>
                            </View>
                        </View>
                    )
                })
            }
        </Fragment>
    )
}

