import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    transactionItem: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 10,
        paddingBottom: 10

    },
    mainText: {
        marginLeft: 20
    },
    textHead: {
        fontWeight: 'bold',
        color: '#000'
    },
    textFoot: {
        fontSize: 12,
    },
    amountText: {
        color: '#000'
    },
    imageContainer: {
        flexDirection: 'row'
    },
    icon: {
        marginLeft: 8
    }
})