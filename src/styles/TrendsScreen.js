import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f0ff',
    },
    tabs: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: 3,
        elevation: 5,
        shadowOpacity: 5,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
    },
    activeTab: {
        borderBottomColor: '#00004d',
        borderBottomWidth: 3,
        paddingBottom: 5
    },
    transactionContainer: {
        flex:1,
        paddingLeft: 10,
        paddingRight: 10,
    },
    transactionList: {
        paddingTop: 20
    },
    dateText: {
        fontSize: 10,
        fontWeight: 'bold',
        paddingBottom: 3
    },
    transactionMain: {
        paddingBottom:20
    },
    footer: {
        backgroundColor:'transparent',
        paddingBottom:20,
        paddingRight:20,
        paddingTop:10
    },
    footerMain: {
        flexDirection:'row',
        justifyContent: 'flex-end'
    },
    footerContent: {
        flexDirection:'row'
    },
    footerImage: {
        marginRight: 20
    }
})