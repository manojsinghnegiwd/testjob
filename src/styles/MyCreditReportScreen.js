import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e6f0ff',
        paddingLeft: 10,
        paddingRight: 10,

    },
    creditScore: {
        flex: 3,
        backgroundColor: '#fff',
        paddingLeft: 25,
        paddingRight: 25,
        justifyContent: 'center',
        marginTop: 20,
        borderRadius:10
    },
    yourCredit: {
        flex: 1,
        paddingLeft: 25,
        paddingRight: 25,
        justifyContent: 'center',
    },
    connect: {
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 35,
        paddingRight: 35,
        width:'100%'
    },
    questionMark: {
        fontSize: 60,
        fontWeight: 'bold',
    },
    unknown: {
        fontSize: 14,
        color: '#D3D3D3',
        fontWeight: 'bold',
    },
    yourCreditText: {
        color: 'grey',
        textAlign: 'center',
        fontSize: 21,
        paddingTop: 25,

    },
    connectBtn: {
        backgroundColor: '#00004d',
        paddingTop: 15,
        paddingBottom: 15,
        width:'100%',
        textAlign:'center',
        borderRadius:25
    },
    connectBtnText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    poorExcellent: {
        justifyContent:'space-between',
        flexDirection:'row'
    },
    poorExcellentText: {
        fontSize:10,
        fontWeight: 'bold',
        color:'#D3D3D3'
    },
    unknownBox: {
        flexDirection:'row',
        width:'100%',
        marginTop:10,
        marginBottom:10
    },
    box1: {
        width:90,
        backgroundColor:'grey',
        height:25,
        marginRight:3,
        borderTopLeftRadius:15,
        borderBottomLeftRadius:15
    },
    box2: {
        width:80,
        backgroundColor:'grey',
        height:25,
        marginRight:3
    },
    box3: {
        width:60,
        backgroundColor:'grey',
        height:25,
        marginRight:3
    },
    box4: {
        width:50,
        backgroundColor:'grey',
        height:25,
        borderTopRightRadius:15,
        borderBottomRightRadius:15
    }
})