import React from 'react'

import Router from '../views/Router'

class App extends React.Component {
    render() {
        return <Router />
    }
}

export default App
