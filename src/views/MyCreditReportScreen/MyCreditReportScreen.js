import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { poorExcellentList } from '../../constants'
import { styles } from '../../styles/MyCreditReportScreen'

export default class MyCreditReportScreen extends Component {
    static navigationOptions = {
        title: 'MY CREDIT REPORT',
        headerTitleStyle: {
            color: 'grey',
            fontSize: 16
        },
        headerLeft: (
            <Image
                source={require('../../assests/images/menu.png')}
                style={{ marginLeft: 8 }}
            />
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.creditScore}>
                    <View style={styles.creditScoreContent}>
                        <Text style={styles.questionMark}>???</Text>
                        <Text style={styles.unknown}>Unknown credit score</Text>
                        <View style={styles.unknownBox}>
                            <View style={styles.box1}></View>
                            <View style={styles.box2}></View>
                            <View style={styles.box3}></View>
                            <View style={styles.box4}></View>
                        </View>
                        <View style={styles.poorExcellent}>
                            {
                                poorExcellentList.map((item, index) => {
                                    return (
                                        <View key={index}>
                                            <Text style={styles.poorExcellentText}>
                                                {item.amount}
                                            </Text>
                                            <Text style={styles.poorExcellentText}>
                                                {item.title}
                                            </Text>
                                        </View>
                                    )
                                })
                            }
                        </View>
                    </View>
                </View>
                <View style={styles.yourCredit}>
                    <Text style={styles.yourCreditText}>Your credit report has not yet connected</Text>
                </View>
                <View style={styles.connect}>
                    <TouchableOpacity onPress={() => this.props.navigation.push('TrendsScreen')} style={styles.connectBtn}>
                        <Text style={styles.connectBtnText}>CONNECT YOUR CREDIT</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}