import { createStackNavigator, createAppContainer } from "react-navigation"

import MyCreditReportScreen from './MyCreditReportScreen/MyCreditReportScreen'
import TrendsScreen from './TrendsScreen/TrendsScreen'

const AppNavigator = createStackNavigator({
    MyCreditReportScreen: {
      screen: MyCreditReportScreen,
    },
    TrendsScreen: {
      screen: TrendsScreen,
    },
  }, {
      initialRouteName: 'MyCreditReportScreen',
  });

const Router = createAppContainer(AppNavigator)

export default Router
