import React, { Component, Fragment } from 'react'
import { View, Text, Image, ScrollView } from 'react-native'
import { TrendsList } from '../../components/TrendsList/TrendsList'
import { trendsList } from '../../constants'
import { styles } from '../../styles/TrendsScreen'

export default class TrendsScreen extends Component {
    static navigationOptions = {
        headerStyle: {
            elevation: 0,
            shadowOpacity: 0
        },
        headerTitle: (
            <View>
                <Text style={{ fontSize: 14, fontWeight: 'bold' }}>
                    SPENDING BEHAVIOUR & TRENDS
                </Text>
                <Text style={{ fontSize: 8, fontWeight: 'bold' }}>
                    Jan 01,2019 to Feb 28,2019
                </Text>
            </View>
        ),
        headerTitleStyle: {
            color: 'grey',
        },
        headerLeft: (
            <Image
                source={require('../../assests/images/menu.png')}
                style={{ marginLeft: 8 }}
            />
        ),
        headerRight: (
            <Fragment>
                <Image
                    source={require('../../assests/images/search.png')}
                    style={{ marginRight: 8 }}
                />
                <Image
                    source={require('../../assests/images/filter.png')}
                    style={{ marginRight: 8 }}
                />
            </Fragment>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.tabs}>
                    <View>
                        <Text style={styles.activeTab}>TRANSACTION</Text>
                    </View>
                    <View>
                        <Text>CHARTS</Text>
                    </View>
                    <View>
                        <Text>CALENDAR</Text>
                    </View>
                    <View>
                        <Text>MEMBERS</Text>
                    </View>
                </View>
                <ScrollView>
                    <View style={styles.transactionMain}>
                        <View style={styles.transactionContainer}>
                            <View style={styles.transactionList} >
                                <Text style={styles.dateText}>FEB 01, 2019</Text>
                                <TrendsList trendsList={trendsList}/>
                            </View>
                        </View>
                        <View style={styles.transactionContainer}>
                            <View style={styles.transactionList} >
                                <Text style={styles.dateText}>FEB 01, 2019</Text>
                                <TrendsList trendsList={trendsList}/>
                            </View>
                        </View>
                        <View style={styles.transactionContainer}>
                            <View style={styles.transactionList} >
                                <Text style={styles.dateText}>FEB 01, 2019</Text>
                                <TrendsList trendsList={trendsList}/>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.footer}>
                    <View style={styles.footerMain}>
                        <View style={styles.footerContent}>
                        <Image
                            source={require('../../assests/images/add.png')}
                            style={styles.footerImage}
                        />
                        <Image
                            source={require('../../assests/images/check-mark.png')}
                        />
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}